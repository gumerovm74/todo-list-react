import React from 'react';

import './spinner.css';

const Spinner = () => {
  return (
    <div className="loadingio-spinner-double-ring-9277h4uuc6a">
      <div className="ldio-bsopsbkxidh">
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Spinner;
